import segyio
import numpy as np
import pandas as pd 
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
from ipywidgets import Button, Layout
from IPython.display import Javascript, display
import traitlets
from ipywidgets import widgets
from IPython.display import display
from tkinter import Tk, filedialog

import traitlets
from IPython.display import display
from ipywidgets import widgets
from tkinter import Tk, filedialog


def create_widgets(f):
    #Control widgets
    xl = widgets.IntSlider(
        min=min(f.xlines),
        max=max(f.xlines),
        step=1,
        description='Xline:',
        disabled=False,
        continuous_update=False,
        orientation='horizontal',
        readout=True,
        readout_format='d'
    )

    il = widgets.IntSlider(
        min=min(f.ilines),
        max=max(f.ilines),
        step=1,
        description='Inline:',
        disabled=False,
        continuous_update=False,
        orientation='horizontal',
        readout=True,
        readout_format='d'
    )

    ds = widgets.IntSlider(
        min=min(f.samples),
        max=max(f.samples),
        step=(max(f.samples)-min(f.samples))/(len(f.samples)-1),
        description='ms:',
        disabled=False,
        continuous_update=False,
        orientation='horizontal',
        readout=True,
        readout_format='d'
    ) 


    sc = widgets.FloatSlider(
        min=0.,
        max=1.,
        step=0.1,
        description='Scale colors:',
        disabled=False,
        continuous_update=False,
        orientation='horizontal',
        readout=True,
        readout_format='.1f'
    ) 
    return xl, il, ds, sc


def create_values(f):
    values = []
    for trace in f.xline[int(((max(f.xlines)-min(f.xlines))/2)+min(f.xlines))]:
        values.extend(trace)
    for trace in f.iline[int(((max(f.ilines)-min(f.ilines))/2)+min(f.ilines))]:
        values.extend(trace)
    return pd.Series(values)




    
def plot_inline(inline, scalar):
    plt.figure(figsize=(10,10))
    norm = mpl.colors.Normalize(vmin=-max(values)*scalar,vmax=max(values)*scalar)
    plt.imshow(f.iline[inline].T, cmap='seismic', vmin=-max(values)*scalar,vmax=max(values)*scalar) 
    plt.colorbar()
    plt.show()
    
    
def plot_xline(xline, scalar):
    plt.figure(figsize=(10,10))
    norm = mpl.colors.Normalize(vmin=-max(values)*scalar,vmax=max(values)*scalar)
    plt.imshow(f.xline[xline].T, cmap='seismic', vmin=-max(values)*scalar,vmax=max(values)*scalar) 
    plt.colorbar()
    plt.show()
    
    
def plot_slice(dslice, scalar):
    plt.figure(figsize=(10,10))
    norm = mpl.colors.Normalize(vmin=-max(values)*scalar,vmax=max(values)*scalar)
    plt.imshow(f.depth_slice[np.where(f.samples==dslice)[0][0]], cmap='seismic', vmin=-max(values)*scalar,vmax=max(values)*scalar)
    plt.colorbar()
    plt.show()
    
def plt_hist(scalar, values):
    plt.figure(figsize=(7,5))
    plt.hist(values[values>=0][(values < values.max()*sc.value)], bins=20, color='red')
    plt.hist(values[values<=0][(values > values.min()*sc.value)], bins=20, color='blue')
    plt.title('\n\n\nValue distribution', fontsize=25)
    plt.grid()
    




from ipywidgets import Button, GridBox, Layout, ButtonStyle

def create_plot(xl, il, ds, sc, values):

    header  = interactive(plot_xline, xline=xl, scalar=sc)

    main    = interactive(plot_inline, inline=il, scalar=sc)

    sidebar = interactive(plot_slice, dslice=ds, scalar=sc)

    footer  = interactive(plt_hist, scalar=sc, values=fixed(values))

    show = GridBox(children=[header, main, sidebar, footer],
            layout=Layout(
                width='100%',
                grid_template_rows='auto auto',
                grid_template_columns='50% 50%',
                grid_template_areas='''
                "header main"
                "sidebar footer"
                ''')
           )
    return show



#run button
def run_all(ev):
    display(Javascript('IPython.notebook.execute_cells_below()'))

button = widgets.Button(description="Read file and run notebook")
button.on_click(run_all)


#validate input
def check_and_read_input(file):
    if '.segy' not in file:
        raise TypeError('Segy file not valid. Please Enter a valid segy file. Use drop down list or type infile name.')
    f = segyio.open(file)
    
    print(' \nLoaded file: ',str(f.attributes).split("'")[1],'\n')
    print('--- Inlines:\n  --- Number of inlines: ',len(f.ilines),'\n  --- Inline range: ',min(f.ilines),'-',max(f.ilines))
    print(' \n--- Xlines:\n  --- Number of xlines: ',len(f.xlines),'\n  --- Xline range: ',min(f.xlines),'-',max(f.xlines))
    print(' \n--- Time/Depth samples:\n  --- Number of Time/Depth samples: ',len(f.samples),'\n  --- Time/Depth range: ',min(f.samples),'-',max(f.samples),'\n  --- Time/Depth step: ',pd.Series(f.samples).diff()[pd.Series(f.samples).diff().notnull()].unique()[0]) 

    return f
    
    
##https://gist.github.com/draperjames/90403fd013e332e4a14070aab3e3e7b0#file-jupyter_notebook_select_files_button-py




class SelectFilesButton(widgets.Button):
    """A file widget that leverages tkinter.filedialog."""

    def __init__(self, *args, **kwargs):
        """Initialize the SelectFilesButton class."""
        super(SelectFilesButton, self).__init__(*args, **kwargs)
        # Add the selected_files trait
        self.add_traits(files=traitlets.traitlets.List())
        # Create the button.
        self.description = "Select Segy file"
        self.icon = "square-o"
        self.style.button_color = "orange"
        # Set on click behavior.
        self.on_click(self.select_files)

    @staticmethod
    def select_files(b):
        """Generate instance of tkinter.filedialog.
        Parameters
        ----------
        b : obj:
            An instance of ipywidgets.widgets.Button
        """
        # Create Tk root
        root = Tk()
        # Hide the main window
        root.withdraw()
        # Raise the root to the top of all windows.
        root.call('wm', 'attributes', '.', '-topmost', True)
        # List of selected fileswill be set to b.value
        b.files = filedialog.askopenfilename(multiple=True)

        b.description = "Files Selected"
        b.icon = "check-square-o"
        b.style.button_color = "lightgreen"
        

get_input = SelectFilesButton()